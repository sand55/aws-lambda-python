#!/bin/bash -e
# *** deploy cloudformation yaml
aws cloudformation package --template-file serverless.yml --output-template-file serverless-output.yml --s3-bucket cf-template-takeda
aws cloudformation deploy --template-file serverless-output.yml --stack-name takeda2 --capabilities CAPABILITY_IAM
